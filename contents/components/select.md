---
name: Select
description: An HTML element, typically used in a form.
componentLabel: form-select
---

## Examples

<story-viewer component="base-form-form-select" title="Select"></story-viewer>

## Structure

<todo>Add structure image.</todo>

## Guidelines

<todo>Add guidelines.</todo>

### Appearance

<todo>Add appearance.</todo>

### Behavior

<todo>Add behavior.</todo>

### Accessibility

<todo>Add accessibility.</todo>
