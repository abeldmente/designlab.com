---
name: Textarea
description: A component for the HTML textarea element.
componentLabel: form-textarea
---

## Examples

<story-viewer component="base-form-form-textarea" title="Textarea"></story-viewer>

## Structure

<todo>Add structure image.</todo>

## Guidelines

<todo>Add guidelines.</todo>

### Appearance

<todo>Add appearance.</todo>

### Behavior

<todo>Add behavior.</todo>

### Accessibility

<todo>Add accessibility.</todo>
