---
name: Typography
---

## Primary typeface

[Inter](https://fonts.google.com/specimen/Inter?preview.text=Inter&preview.text_type=custom&query=Inter) is the default typeface for all GitLab materials. It is an open source font and features a tall x-height to aid in readability. Although it comes in an array of weights, we only use Semi Bold and Regular to preserve simplicity.

### Usage

Refer to the guidelines below when working with typography:

- Alternate between different sizes and weights to establish layout hierarchy.
- Keep the font size consistent within each block of copy.
- Left-align all copy. Never force-justify, center-align, or right-align typography, unless the written language dictates otherwise.
- Default to sentence case unless working with a tagline/headline or different tiers of information.
- No ALL-CAPS please.
- Keep text solid-filled and refrain from adding strokes to outline the type.
- Display headlines should use Inter Semi Bold with 105% leading and -45 tracking (-4.5% in Figma).
- Subheaders and intro/outro paragraphs should use Inter Regular with 110% leading, -20 tracking (-2% in Figma), and optical kerning.
- Body copy should use Inter Regular with 135% leading, 0 tracking, and auto kerning.
- Captions and labels should use Inter Italic with 135% leading, 0 tracking, and auto kerning.
- Call-to-actions and buttons should use Inter Semi Bold.

<figure-img alt="Typography samples" label="Inter typography samples" src="/img/brand/typography.svg"></figure-img>

## Type repetitions

Type repetitions are a stylistic expression of GitLab’s commitment to iteration. It is reserved for moments of emphasis and adding visual interest.

<figure-img alt="Type repetitions mockup" label="Type repetitions mockup" src="/img/brand/devops-shirt-mockup.png" width="480"></figure-img>

### Construction

Follow these guidelines to properly achieve the type repetitions effect:

1. Type out a single word in Inter Semi Bold.
1. Create a copy of the text and remove the bottom 1/4 of the text from the baseline.
1. Take this newly redacted type and repeat it at least 3 times above the original text.
1. Ensure that the vertical spacing between each repetition equals 75% of the bottom portion that was removed.
1. Keep the font weight and color the same throughout, although opacity changes can be applied to the repetitions.

<figure-img alt="Type repetitions" label="Type repetition settings" src="/img/brand/type-repetitions-construction.svg"></figure-img>
